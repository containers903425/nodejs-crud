//khai báo mongoose
const mongoose = require("mongoose");

// Khai báo class Schema 
const Schema = mongoose.Schema;

// Khởi tạo 1 instance orderSchema từ Class Schema
const orderSchema = new Schema({
    customer: {
        type: mongoose.Types.ObjectId,
        ref: "User",
        required: true
    },
    products: [{
        product: {
            type: mongoose.Types.ObjectId,
            ref: "Product",
            required: true
        },
        quantity: {
            type: Number,
            required: true
        },
        price: {
            type: Number,
            required: true
        }
    }],
    status: {
        type: String,
        enum: ["Pending", "Confirmed", "Cancelled"],
        default: "Pending"
    },
    total: {
        type: Number,
        required: true
    },
    payment: {
        type: mongoose.Types.ObjectId,
        ref: "Payment"
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model("Order", orderSchema);