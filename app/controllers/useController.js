//import model
const userModel = require("../models/userModel");

// Create a user - Tạo mới user
const createUser= async (req, res)=> {
  try {
    const { fullName, email, password, phone, address, role } = req.body;

    // Kiểm tra email và phone đã tồn tại trong database chưa
    const emailExists = await userModel.findOne({ email });
    const phoneExists = await userModel.findOne({ phone });
    if (emailExists) {
      return res.status(400).json({ message: "Email already exists" });
    }
    if (phoneExists) {
      return res.status(400).json({ message: "Phone already exists" });
    }

    // Tạo một instance user mới từ model User
    const user = new userModel({
      fullName,
      email,
      password,
      phone,
      address,
      role
    });

    // Lưu user vào database
    const savedUser = await user.save();

    res.status(201).json(savedUser);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
}

// Get all user - Tải danh sách user
const getAllUser = async (req, res) =>{
  try {
    const users = await userModel.find();
    res.json(users);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
}

// Get a user by Id
const getUserById = async (req, res) => {
  try {
    const user = await userModel.findById(req.params.id);
    if (!user) {
      return res.status(404).json({ message: "User not found" });
    }
    res.json(user);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};
  
// Update user
const updateUserById= async(req, res)=> {
  try {
    const userId = req.params.id;
    const { fullName, email, password, phone, address, role } = req.body;

    // Check if user with given ID exists
    const user = await userModel.findById(userId);
    if (!user) {
      return res.status(404).json({ message: "User not found" });
    }

    // Update user's properties
    user.fullName = fullName;
    user.email = email;
    user.password = password;
    user.phone = phone;
    user.address = address;
    user.role = role;

    // Save updated user to database
    const updatedUser = await user.save();

    res.status(200).json(updatedUser);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
}
  

// Delete user by Id
const deleteUserById = async (req, res) => {
  try {
    const userId = req.params.id;

    // Kiểm tra nếu user tồn tại trong database
    const user = await userModel.findById(userId);
    if (!user) {
      return res.status(404).json({ message: "User not found" });
    }

    // Xóa user khỏi database
    await userModel.findByIdAndDelete(userId);

    res.json({ message: "User deleted successfully" });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
}


module.exports = {
    createUser,
    getAllUser,
    getUserById,
    updateUserById,
    deleteUserById,
}
