//import model
const promotionModel = require("../models/promotionModel");

const createPromotion= async(req, res)=> {
    try {
      const { code, discount, expiredAt } = req.body;
  
      // Create a new promotion instance from the Promotion model
      const promotion = new promotionModel({
        code,
        discount,
        expiredAt
      });
  
      // Save the new promotion to the database
      const savedPromotion = await promotion.save();
  
      res.status(201).json(savedPromotion);
    } catch (error) {
      res.status(400).json({ message: error.message });
    }
  }

 const getAllPromotions= async(req, res)=> {
    try {
      const promotions = await promotionModel.find();
      res.status(200).json(promotions);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  }

  const getPromotionById= async(req, res)=> {
    try {
      const promotion = await promotionModel.findById(req.params.id);
  
      if (!promotion) {
        return res.status(404).json({ message: 'Promotion not found' });
      }
  
      res.json(promotion);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  }


  const updatePromotionById=async(req, res) =>{
    try {
      const { code, discount, expiredAt } = req.body;
      const updatedPromotion = await promotionModel.findByIdAndUpdate(
        req.params.id,
        { code, discount, expiredAt, updatedAt: Date.now() },
        { new: true }
      );
      res.status(200).json(updatedPromotion);
    } catch (error) {
      res.status(400).json({ message: error.message });
    }
  }

  const deletePromotionById = async (req, res) => {
    try {
      const { id } = req.params;
  
      const deletedPromotion = await promotionModel.findByIdAndDelete(id);
  
      if (!deletedPromotion) {
        return res.status(404).json({ message: "Promotion not found" });
      }
  
      res.status(200).json({ message: "Promotion deleted successfully" });
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  };

  module.exports = {
    createPromotion,
    getAllPromotions,
    getPromotionById,
    updatePromotionById,
    deletePromotionById,
}

